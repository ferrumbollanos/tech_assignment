# Licensed to the Apache Software Foundation (ASF) under one or more
# contributor license agreements.  See the NOTICE file distributed with
# this work for additional information regarding copyright ownership.
# The ASF licenses this file to You under the Apache License, Version 2.0
# (the "License"); you may not use this file except in compliance with
# the License.  You may obtain a copy of the License at
#
#    http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
FROM apache/airflow:2.1.2-python3.8

SHELL ["/bin/bash", "-o", "pipefail", "-e", "-u", "-x", "-c"]

ARG AIRFLOW__CORE__EXECUTOR
ENV AIRFLOW__CORE__EXECUTOR $AIRFLOW__CORE__EXECUTOR
ARG AIRFLOW__CORE__SQL_ALCHEMY_CONN
ENV AIRFLOW__CORE__SQL_ALCHEMY_CONN $AIRFLOW__CORE__SQL_ALCHEMY_CONN
ARG AIRFLOW__CELERY__RESULT_BACKEND
ENV AIRFLOW__CELERY__RESULT_BACKEND $AIRFLOW__CELERY__RESULT_BACKEND
ARG AIRFLOW__CELERY__BROKER_URL
ENV AIRFLOW__CELERY__BROKER_URL $AIRFLOW__CELERY__BROKER_URL
ARG AIRFLOW__CORE__FERNET_KEY
ENV AIRFLOW__CORE__FERNET_KEY $AIRFLOW__CORE__FERNET_KEY
ARG AIRFLOW__CORE__DAGS_ARE_PAUSED_AT_CREATION
ENV AIRFLOW__CORE__DAGS_ARE_PAUSED_AT_CREATION $AIRFLOW__CORE__DAGS_ARE_PAUSED_AT_CREATION
ARG AIRFLOW__CORE__LOAD_EXAMPLES
ENV AIRFLOW__CORE__LOAD_EXAMPLES $AIRFLOW__CORE__LOAD_EXAMPLES
ARG AIRFLOW__API__AUTH_BACKEND
ENV AIRFLOW__API__AUTH_BACKEND $AIRFLOW__API__AUTH_BACKEND
ARG AIRFLOW_CONN_GOOGLE_CLOUD_DEFAULT
ENV AIRFLOW_CONN_GOOGLE_CLOUD_DEFAULT $AIRFLOW_CONN_GOOGLE_CLOUD_DEFAULT

RUN pip install backoff
