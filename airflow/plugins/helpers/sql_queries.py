"""
SQL Queries

Pushes new data to warehouse
"""

staging_to_prod_review = """
INSERT INTO review (product_id, unix_timestamp, reviewer_pk, helpful, vote)
(SELECT p.productpk as product_id, unix_timestamp, r.reviewerpk as reviewer_pk, helpful, vote
    FROM staging_review as strw
    LEFT JOIN product p on strw.asin = p.asin
    LEFT JOIN reviewer r on strw.reviewer_id = r.reviewer_id
    WHERE concat(concat(p.asin, strw.reviewer_id), strw.unix_timestamp::text) NOT IN (SELECT concat(concat(cp.asin, r.reviewer_id), strw.unix_timestamp::text)
                                                                        FROM review
                                                                        LEFT JOIN product cp on review.product_id = cp.productpk
                                                                        LEFT JOIN reviewer r on review.reviewer_pk = r.reviewerpk))
"""

staging_to_prod_product = """
INSERT INTO product (title, asin, brand, price)
(SELECT title, asin, brand, price
    FROM staging_product as stpd
    WHERE asin NOT IN (SELECT asin
                        FROM product))
"""

staging_to_prod_reviewer = """
INSERT INTO reviewer (reviewer_id, reviewer_name)
(SELECT reviewer_id, reviewer_name
    FROM staging_reviewer as strr
    WHERE reviewer_id NOT IN (SELECT reviewer_id
                                FROM reviewer))
"""

staging_to_prod_date = """
INSERT INTO date (unix_timestamp, hour, day, month, year)
(SELECT unix_timestamp, hour, day, month, year
    FROM staging_date as stdt
    WHERE unix_timestamp NOT IN (SELECT unix_timestamp
                                    FROM date))
"""
