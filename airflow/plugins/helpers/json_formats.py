jsonpaths = {
    "products": {"jsonpaths": ["$['title']", "$['asin']", "$['brand']", "$['price']"]},
    "reviewer": {"jsonpaths": ["$['reviewerID']", "$['reviewerName']"]},
    "review": {
        "jsonpaths": [
            "$['unixReviewTime']",
            "$['asin']",
            "$['reviewerID']",
            "$['helpful_parsed']",
            "$['overall']",
        ]
    },
    "date": {
        "jsonpaths": [
            "$['unixReviewTime']",
            "$['hour']",
            "$['day']",
            "$['month']",
            "$['year']",
        ]
    },
}
