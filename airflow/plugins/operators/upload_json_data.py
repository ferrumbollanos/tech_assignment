import gzip
import json
import time
from datetime import datetime
from typing import List

import boto3
import pandas as pd
from airflow.models import BaseOperator


class UploadJsonDataOperator(BaseOperator):
    """
    Process data from a json.gz {source} and sends it to s3

    :param string source: File path
    :param string s3_bucket: AWS S3 bucket
    :param string s3_path: AWS S3 path to save the files
    :param string s3_region: AWS Region
    :param string aws_access_key_id: AWS KEY
    :param string aws_secret_access_key: AWS SECRET
    """

    template_fields = [
        "source",
    ]

    def __init__(
        self,
        source: str,
        s3_bucket: str,
        s3_path: str,
        s3_region: str,
        aws_access_key_id: str,
        aws_secret_access_key: str,
        *args,
        **kwargs,
    ):
        super().__init__(*args, **kwargs)
        self.source = source
        self.s3_bucket = s3_bucket
        self.s3_path = s3_path
        self.s3_region = s3_region
        self.aws_access_key_id = aws_access_key_id
        self.aws_secret_access_key = aws_secret_access_key

        # create s3 resource
        self.s3 = boto3.resource(
            "s3",
            region_name=s3_region,
            aws_access_key_id=aws_access_key_id,
            aws_secret_access_key=aws_secret_access_key,
        )

    def execute(self, context):
        self.log.info("CleanDataOperator begin execute")

        # Start timer to check how long this is taking
        start_time = time.time()

        json_data_stream = self.parse(self.source)

        json_filename = self.source.replace(".json.gz", ".json")
        with open(json_filename, "a+") as f:
            for data in json_data_stream:
                f.write(json.dumps(data))

        self.upload_files_to_S3([json_filename], self.s3_path, self.s3_bucket)

        self.log.info(f"CleanDataOperator ended in {time.time() - start_time} seconds")

    def upload_files_to_S3(self, filenames, path, bucket_name):
        for file in filenames:
            filename = file.rsplit("/", 1)[-1]
            self.s3.Bucket(bucket_name).upload_file(file, f"{path}/{filename}")

    @staticmethod
    def process(data):
        if "helpful" in data.keys():
            data["helpful_parsed"] = data["helpful"][0] / (
                data["helpful"][1] if data["helpful"][1] > 0 else 1
            )
            date = datetime.fromtimestamp(data["unixReviewTime"])
            data["hour"], data["day"], data["month"], data["year"] = (
                date.hour,
                date.day,
                date.month,
                date.year,
            )
        return data

    def parse(self, path: str):
        include_keys = [
            "title",
            "asin",
            "brand",
            "price",
            "reviewerID",
            "reviewerName",
            "unixReviewTime",
            "overall",
            "helpful",
        ]

        with gzip.open(path, "r") as f:
            for index, item in enumerate(f):
                try:
                    data = json.loads(item.decode("utf-8"))
                except json.JSONDecodeError:
                    data = eval(item.decode("utf-8"))
                data = {k: v for k, v in data.items() if k in include_keys}
                yield self.process(data)
