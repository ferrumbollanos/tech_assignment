from airflow.models import BaseOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook


class S3JSONToRedshiftOperator(BaseOperator):
    """
    Populate redshift tables from JSON source files.
    :param string  redshift_conn_id: reference to a specific redshift database
    :param string  table_name: redshift staging table to load
    :param string  s3_bucket: S3 bucket location
    :param string  s3_path: S3 file path
    :param string  aws_key: AWS user key
    :param string  aws_secret: AWS user secret
    :param string  region: S3 bucket location
    :param string  jsonpath: Name of the json format for Redshift
    """

    template_fields = [
        "s3_path",
    ]

    def __init__(
        self,
        redshift_conn_id: str,
        table_name: str,
        truncate_table: bool,
        s3_bucket: str,
        s3_path: str,
        jsonpath: str,
        aws_access_key_id: str,
        aws_secret_access_key: str,
        region: str,
        *args,
        **kwargs,
    ):

        super().__init__(*args, **kwargs)

        self.redshift_conn_id = redshift_conn_id
        self.table_name = table_name
        self.s3_bucket = s3_bucket
        self.s3_path = s3_path
        self.aws_access_key_id = aws_access_key_id
        self.aws_secret_access_key = aws_secret_access_key
        self.region = region
        self.truncate_table = truncate_table
        self.jsonpath = jsonpath


    def execute(self, context):
        self.log.info("S3JSONToRedshiftOperator begin execute")

        # connect to Redshift
        redshift_hook = PostgresHook(postgres_conn_id=self.redshift_conn_id)
        self.log.info(f"Connected with {self.redshift_conn_id}")

        if self.truncate_table:
            self.log.info(f"Truncating table {self.table_name}")
            redshift_hook.run(f"TRUNCATE TABLE {self.table_name}")

            self.log.info(f"{self.table_name} table truncated")

        sql_stmt = f"""
            COPY {self.table_name} 
                FROM 's3://{self.s3_bucket}/{self.s3_path}' 
                ACCESS_KEY_ID '{self.aws_access_key_id}'
                SECRET_ACCESS_KEY '{self.aws_secret_access_key}'
                REGION '{self.region}'
                json 's3://bestsellerassignment/artifacts/jsonpaths/{self.jsonpath}'
                TRUNCATECOLUMNS
        """
        self.log.info(sql_stmt)

        redshift_hook.run(sql_stmt)
        self.log.info(f"S3JSONToRedshiftOperator complete")
