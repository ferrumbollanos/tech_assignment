import gzip
import json
import time
from datetime import datetime
from typing import List

import boto3
import pandas as pd
from airflow.models import BaseOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook


class DedupeRedshiftOperator(BaseOperator):
    """
    Populate redshift tables from staging tables.
    :param string  redshift_conn_id: reference to a specific redshift database
    :param string  table_name: name of the db table
    :param string  fields_to_check: list of the fields to check for composite duplication
    """

    def __init__(
        self,
        redshift_conn_id,
        table_name,
        fields_to_check,
        *args,
        **kwargs,
    ):

        super().__init__(*args, **kwargs)

        self.redshift_conn_id = redshift_conn_id
        self.table_name = table_name
        self.fields_to_check = fields_to_check

    def execute(self, context):
        self.log.info("DedupeRedshiftOperator begin execute")

        # connect to Redshift
        redshift_hook = PostgresHook(postgres_conn_id=self.redshift_conn_id)
        self.log.info(f"Connected with {self.redshift_conn_id}")

        deduplicated_table = f"deduplicated_{self.table_name}"
        sql_query = f"""
            BEGIN;
    
            CREATE TEMP TABLE {deduplicated_table} AS
                SELECT t.*
            FROM
            (SELECT *, ROW_NUMBER() OVER (
                        PARTITION BY {','.join(self.fields_to_check)}
                        ORDER BY {','.join(self.fields_to_check)}
                    ) row_num
                FROM
                    {self.table_name}) t
            WHERE row_num = 1;
            
            ALTER TABLE {deduplicated_table} DROP COLUMN row_num;
            
            TRUNCATE TABLE {self.table_name};
            
            INSERT INTO {self.table_name} (SELECT * FROM {deduplicated_table});
            
            DROP TABLE {deduplicated_table};
            
            COMMIT;
        """

        self.log.info(sql_query)
        redshift_hook.run(sql_query)

        self.log.info(f"DedupeRedshiftOperator complete")