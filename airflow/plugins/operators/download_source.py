import os

import backoff
import requests
from airflow.models import BaseOperator


class DownloadSourceOperator(BaseOperator):
    """
    Downloads a {source} into a {output_file}
    Handles partial downloads between task tries

    :param string source: source url to download
    :param string output_file: file to downloads the contents into
    """

    template_fields = [
        "output_file",
    ]

    def __init__(self, source: str, output_file: str, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.source = source
        self.output_file = output_file

    def execute(self, context):
        if not self.source.endswith(".json.gz"):
            raise Exception("Source file to download is not of extension .json.gz")

        self.log.info("DownloadSourceOperator begin execute")
        try:
            self.make_download()
        except requests.exceptions.ConnectionError:
            self.log.error(
                "Error with connection, download will continue on next task try"
            )
            raise
        self.log.info(
            f"DownloadSourceOperator ended -- downloaded into file {self.output_file}"
        )

    @backoff.on_exception(
        backoff.expo, requests.exceptions.ConnectionError, max_tries=5
    )
    def make_download(self):
        # initialize current_size here to allow for partial downloads to resume
        current_percentage = -1
        try:
            current_size = os.stat(self.output_file)
        except FileNotFoundError:
            current_size = 0

        with open(self.output_file, "ab+") as f, requests.get(
            self.source,
            stream=True,
            timeout=10,
            headers={"Range": f"bytes={current_size}-"},
        ) as r:
            size = int(r.headers["Content-Length"])
            for chunk in r.iter_content(1024):
                if chunk:
                    f.write(chunk)

                    # Logging progress of download
                    f.seek(0, os.SEEK_END)
                    # Check for prev_percentage so it only logs a percentage once
                    prev_percentage = current_percentage
                    current_size = f.tell()
                    current_percentage = int(current_size / size * 100)

                    # We'll check if current and prevous percentage are different and
                    # only log multiples of 5 to save on logging space
                    if (
                        prev_percentage != current_percentage
                        and current_percentage % 5 == 0
                    ):
                        self.log.info(
                            f"Progress ---- {current_percentage}% ---- {current_size} MB/{size} MB ----",
                        )
