from airflow.models import BaseOperator
from airflow.providers.postgres.hooks.postgres import PostgresHook


class InsertToRedshiftOperator(BaseOperator):
    """
    Populate redshift tables from staging tables.
    :param string  redshift_conn_id: reference to a specific redshift database
    :param string  insert_query: insert query
    """

    def __init__(
        self,
        redshift_conn_id,
        insert_query,
        *args,
        **kwargs,
    ):

        super().__init__(*args, **kwargs)

        self.redshift_conn_id = redshift_conn_id
        self.insert_query = insert_query

    def execute(self, context):
        self.log.info("InsertToRedshiftOperator begin execute")

        # connect to Redshift
        redshift_hook = PostgresHook(postgres_conn_id=self.redshift_conn_id)
        self.log.info(f"Connected with {self.redshift_conn_id}")

        # Run selected query
        self.log.info(self.insert_query)
        redshift_hook.run(self.insert_query)

        self.log.info(f"InsertToRedshiftOperator complete")
