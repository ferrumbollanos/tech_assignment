CREATE TABLE IF NOT EXISTS product (
    productpk INT IDENTITY(0,1),
    title varchar(600),
    asin varchar(255),
    brand varchar(255),
    price float,
    PRIMARY KEY(productpk)
);

CREATE TABLE IF NOT EXISTS staging_product (
    title varchar(600),
    asin varchar(255),
    brand varchar(255),
    price float
);

CREATE TABLE IF NOT EXISTS reviewer (
    reviewerpk INT IDENTITY(0,1),
    reviewer_id varchar(255),
    reviewer_name varchar(255),
    PRIMARY KEY(reviewerpk)
);

CREATE TABLE IF NOT EXISTS staging_reviewer (
    reviewer_id varchar(255),
    reviewer_name varchar(255)
);

CREATE TABLE IF NOT EXISTS "date" (
    unix_timestamp bigint NOT NULL,
    "hour" int4,
    "day" int4,
    "month" int4,
    "year" int4,
    PRIMARY KEY(unix_timestamp)
);

CREATE TABLE IF NOT EXISTS "staging_date" (
    unix_timestamp bigint NOT NULL,
    "hour" int4,
    "day" int4,
    "month" int4,
    "year" int4
);

CREATE TABLE IF NOT EXISTS review (
    product_id INT,
    unix_timestamp INT,
    reviewer_pk INT,
    helpful FLOAT,
    vote FLOAT,

    CONSTRAINT fk_product
       FOREIGN KEY(product_id)
       REFERENCES product(productpk),
    CONSTRAINT fk_reviewer
       FOREIGN KEY(reviewer_pk)
       REFERENCES reviewer(reviewerpk),
    CONSTRAINT fk_date
       FOREIGN KEY(unix_timestamp)
       REFERENCES date(unix_timestamp)
);

CREATE TABLE IF NOT EXISTS staging_review (
    unix_timestamp INT,
    asin varchar(500),
    reviewer_id varchar(255),
    helpful FLOAT,
    vote FLOAT
);
