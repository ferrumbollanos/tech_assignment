import json
import os
import datetime

import boto3
from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.providers.amazon.aws.hooks.base_aws import AwsBaseHook
from airflow.operators.dummy import DummyOperator
from airflow.providers.postgres.operators.postgres import PostgresOperator

from operators.upload_json_data import UploadJsonDataOperator
from operators.download_source import DownloadSourceOperator
from operators.s3_to_redshift import S3JSONToRedshiftOperator
from operators.deduplication_operator import DedupeRedshiftOperator
from operators.insert_to_redshift import InsertToRedshiftOperator

from helpers import sql_queries

from helpers.json_formats import jsonpaths

REDSHIFT_CONN_ID = "redshift"
AWS_KEY = AwsBaseHook("aws_credentials", client_type="s3").get_credentials().access_key
AWS_SECRET = (
    AwsBaseHook("aws_credentials", client_type="s3").get_credentials().secret_key
)
AWS_REGION = "us-east-1"
S3_BUCKET = "bestsellerassignment"
DATA_S3_PATH = "artifacts/table_data"


# Simulation of input
reviews_source = "http://snap.stanford.edu/data/amazon/productGraph/categoryFiles/reviews_Video_Games.json.gz"
meta_source = "http://snap.stanford.edu/data/amazon/productGraph/categoryFiles/meta_Video_Games.json.gz"

# Templated names for temp files
review_output_file = "reviews_{{ dag_run.get_task_instance('start').start_date }}"
meta_output_file = "meta_products_{{ dag_run.get_task_instance('start').start_date }}"


default_args = {
    "owner": "fer",
    "start_date": datetime.datetime(2021, 7, 1),
    "depends_on_past": False,
    "retries": 3,
    "retry_delay": datetime.timedelta(minutes=5),
}

dag = DAG(
    "json_amazon_review_data",
    default_args=default_args,
    description="ETL to data warehouse using json data to S3",
    schedule_interval="0 */3 * * *",
    catchup=False,
)

start = DummyOperator(
    task_id="start",
    dag=dag,
)


def upload_jsonpaths():
    """
    Uploads Json formats to be used in the Redshift COPY statement
    """
    s3 = boto3.client(
        "s3",
        region_name=AWS_REGION,
        aws_access_key_id=AWS_KEY,
        aws_secret_access_key=AWS_SECRET,
    )
    for k, format in jsonpaths.items():
        s3.put_object(
            Body=json.dumps(format).encode("utf-8"),
            Bucket=S3_BUCKET,
            Key=f"{DATA_S3_PATH}/jsonpaths/{k}.json",
        )


upload_jsonpaths_task = PythonOperator(
    task_id="upload_jsonpaths", dag=dag, python_callable=upload_jsonpaths
)

create_tables_task = PostgresOperator(
    task_id="create_tables",
    dag=dag,
    postgres_conn_id=REDSHIFT_CONN_ID,
    sql="create_tables.sql",
)

# We're downloading a gzip as input, one idea I had for this, if we were not parsing a gzip, is to send the data
# directly to s3 - redshift and run computations on staging tables, directly on the database instead.
download_meta = DownloadSourceOperator(
    task_id="download_meta",
    dag=dag,
    source=meta_source,
    output_file="/tmp/" + meta_output_file + ".json.gz",
)

# And we're also downloading full source data and trying to insert it again, This input can be a lot better if they
# were just changes/new data. Maybe we could create a workflow that goes through the data, finds changes, and uploads
# a feed of changed items. And we read the data from that.
download_reviews = DownloadSourceOperator(
    task_id="download_reviews",
    dag=dag,
    source=reviews_source,
    output_file="/tmp/" + review_output_file + ".json.gz",
)

upload_review_data = UploadJsonDataOperator(
    task_id="upload_review_data",
    dag=dag,
    source="/tmp/" + review_output_file + ".json.gz",
    s3_bucket=S3_BUCKET,
    s3_path=DATA_S3_PATH,
    s3_region=AWS_REGION,
    aws_access_key_id=AWS_KEY,
    aws_secret_access_key=AWS_SECRET,
)

upload_meta_data = UploadJsonDataOperator(
    task_id="upload_meta_data",
    dag=dag,
    source="/tmp/" + meta_output_file + ".json.gz",
    s3_bucket=S3_BUCKET,
    s3_path=DATA_S3_PATH,
    s3_region=AWS_REGION,
    aws_access_key_id=AWS_KEY,
    aws_secret_access_key=AWS_SECRET,
)

stage_time = S3JSONToRedshiftOperator(
    task_id="stage_time",
    dag=dag,
    redshift_conn_id=REDSHIFT_CONN_ID,
    table_name="staging_date",
    truncate_table=True,
    s3_bucket=S3_BUCKET,
    s3_path=f"{DATA_S3_PATH}/{review_output_file}.json",
    jsonpath="date.json",
    region=AWS_REGION,
    aws_access_key_id=AWS_KEY,
    aws_secret_access_key=AWS_SECRET,
)

stage_products = S3JSONToRedshiftOperator(
    task_id="stage_products",
    dag=dag,
    redshift_conn_id=REDSHIFT_CONN_ID,
    table_name="staging_product",
    truncate_table=True,
    s3_bucket=S3_BUCKET,
    s3_path=f"{DATA_S3_PATH}/{meta_output_file}.json",
    jsonpath="products.json",
    region=AWS_REGION,
    aws_access_key_id=AWS_KEY,
    aws_secret_access_key=AWS_SECRET,
)

stage_reviewers = S3JSONToRedshiftOperator(
    task_id="stage_reviewers",
    dag=dag,
    redshift_conn_id=REDSHIFT_CONN_ID,
    table_name="staging_reviewer",
    truncate_table=True,
    s3_bucket=S3_BUCKET,
    s3_path=f"{DATA_S3_PATH}/{review_output_file}.json",
    jsonpath="reviewer.json",
    region=AWS_REGION,
    aws_access_key_id=AWS_KEY,
    aws_secret_access_key=AWS_SECRET,
)

stage_reviews = S3JSONToRedshiftOperator(
    task_id="stage_reviews",
    dag=dag,
    redshift_conn_id=REDSHIFT_CONN_ID,
    table_name="staging_review",
    truncate_table=True,
    s3_bucket=S3_BUCKET,
    s3_path=f"{DATA_S3_PATH}/{review_output_file}.json",
    jsonpath="review.json",
    region=AWS_REGION,
    aws_access_key_id=AWS_KEY,
    aws_secret_access_key=AWS_SECRET,
)

dedupe_product = DedupeRedshiftOperator(
    task_id="dedupe_product",
    dag=dag,
    redshift_conn_id=REDSHIFT_CONN_ID,
    table_name="staging_product",
    fields_to_check=["asin"],
)

dedupe_time = DedupeRedshiftOperator(
    task_id="dedupe_time",
    dag=dag,
    redshift_conn_id=REDSHIFT_CONN_ID,
    table_name="staging_date",
    fields_to_check=["unix_timestamp"],
)

dedupe_reviewer = DedupeRedshiftOperator(
    task_id="dedupe_reviewer",
    dag=dag,
    redshift_conn_id=REDSHIFT_CONN_ID,
    table_name="staging_reviewer",
    fields_to_check=["reviewer_id"],
)

dedupe_review = DedupeRedshiftOperator(
    task_id="dedupe_review",
    dag=dag,
    redshift_conn_id=REDSHIFT_CONN_ID,
    table_name="staging_review",
    fields_to_check=["unix_timestamp", "asin", "reviewer_id"],
)

save_date = InsertToRedshiftOperator(
    task_id="save_date",
    dag=dag,
    redshift_conn_id=REDSHIFT_CONN_ID,
    insert_query=sql_queries.staging_to_prod_date,
)

save_product = InsertToRedshiftOperator(
    task_id="save_product",
    dag=dag,
    redshift_conn_id=REDSHIFT_CONN_ID,
    insert_query=sql_queries.staging_to_prod_product,
)

save_reviewer = InsertToRedshiftOperator(
    task_id="save_reviewer",
    dag=dag,
    redshift_conn_id=REDSHIFT_CONN_ID,
    insert_query=sql_queries.staging_to_prod_reviewer,
)

save_reviews = InsertToRedshiftOperator(
    task_id="save_reviews",
    dag=dag,
    redshift_conn_id=REDSHIFT_CONN_ID,
    insert_query=sql_queries.staging_to_prod_review,
)

end_dag = DummyOperator(
    task_id="end_dag",
    dag=dag,
)

start >> create_tables_task >> upload_jsonpaths_task
(
    upload_jsonpaths_task
    >> download_meta
    >> upload_meta_data
    >> stage_products
    >> dedupe_product
    >> save_product
    >> save_reviews
    >> end_dag
)
upload_jsonpaths_task >> download_reviews >> upload_review_data
upload_review_data >> stage_time >> dedupe_time >> save_date >> save_reviews
upload_review_data >> stage_reviewers >> dedupe_reviewer >> save_reviewer >> save_reviews
upload_review_data >> stage_reviews >> dedupe_review >> save_reviews
save_reviews >> end_dag
