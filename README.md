# Data Engineering Assignment

## Assignment

Create a pipeline workflow for extracting data from the JSON files, transforming it into a dimensional 
model and importing it into a database/cluster. 
Some descriptives on the dataset can be found at http://jmcauley.ucsd.edu/data/amazon/links.html 
 
#### Must Haves: 
- Proper exception handling/logging for bulletproof runs (3 times a day) 
- Expressive, re-usable, clean code 
- Include dimensional modeling of data. 
- Contain product price as one of the fields in the dimensional model. 
- Handle duplicates. 
- Download the source data from the pipeline itself and have the ability to do the same at regular intervals. 
- Use some scheduling framework or workflow platform (preferably airflow). 
- Comments of those parts (optimization) where you could speed up execution and/or save network traffic 
 
#### Nice to Haves: 
 
- Handle partial downloads and failures at getting the source data.  
- Be scalable in case new data were to flow in on a high-volume basis(50x bigger) and has to be imported at a higher frequency. 
- Be able to handle the json files as a stream/ abstract the file reading into a streaming model. 
- Describe the data store and tools used to query it - including the presentation layer.

---

#### Some words before I explain the assignment

This was a great assignment for me as someone who is aspiring to be a Data Engineer. I did a bootcamp but I'm more of a hands on kind of guy.

I am developing a personal project myself and doing this assignment really opened my eyes to a lot of things I can do better!

Thank you for the opportunity, and I'm waiting for a positive response!!

Any feedback on this project is HIGHLY appreciated either way.

---

## Data Structure

We'll have a Warehouse with 3 Dimensional tables and 1 Fact table.

#### Dimensional tables:

- date - Date and time
- reviewer - Reviewer information
- product - Has general information about the product

#### Fact tables

- review - Specific review measurable data. (vote, helpfulness)

#### Staging Tables

- staging_review - Staging table to merge to review fact table

## Pipeline

### Tools

- Airflow - Workflow Platform
- S3 - Storage to copy to Redshift and as a plus, a log of files that's been imported to warehouse.
- Redshift - Cluster/Warehouse - Integrations with S3 run really smoothly.

### Workflow
- Create tables if they don't exist
- Download source files (.json.gz)
- Clean data and upload to S3
- Load staging data to Redshift
- Merge staging with Dimension and Fact production tables

## To Run:

### Prerequisites

- Install `Docker` and `docker-compose`
- Make sure airflow volumes folders are writable for docker with `chmod -R 777 airflow/`

### Run

- `docker-compose up airflow-init`
- `docker-compose up`
- Airflow UI is in `127.0.0.1:8080/`
- Create connection to AWS with credentials with permissions for S3 and Redshift. (login: Access Key, Password: Secret)
- Create connection to a Redshift cluster.

## Some other thoughts

- First I tested the pipeline with Dataframes, but it was a big load on a single cpu, I tried with Json and it was a light load on a single computing instance, and it's a cleaner implementation in my opinion but it's got it's down side and it's that it's a heavier load on the database.
- First implementation works if we introduce the use of clusters of computing units.
